plugins {
    kotlin("jvm") version "1.9.20"
}

group = "cz.ctu.fit.bi.kot.hello_word"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.jetbrains.kotlin:kotlin-test")
}

tasks.test {
    useJUnitPlatform()
}
kotlin {
    jvmToolchain(21)
}