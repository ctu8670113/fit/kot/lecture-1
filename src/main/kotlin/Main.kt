package cz.ctu.fit.bi.kot.hello_word


data class Person(val name: String, val age: Int)


fun main() {
    val tom = Person("Tom", 22)
    val tom2 = Person("Tom", 22)
    println(tom)
    println(tom == tom2)
    println(tom === tom2)

}
